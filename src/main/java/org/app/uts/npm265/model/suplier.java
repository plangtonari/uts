/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.app.uts.npm265.model;

import javax.persistence.Column;
import javax.persistence.Id;

/**
 *
 * @author User
 */
public class suplier {
    public class Suplier {
     @Id
    @Column(name = "id_supplier")
    private String id_supplier;
    @Column(name = "nama_suplier")
    private String nama_suplier;
    @Column(name = "alamat")
    private String alamat;
    @Column(name = "kota")
    private String kota;
    @Column(name = "kode_post")
    private String kode_post;
  
    
    public Suplier(){
        
        public Suplier(String cutomerID, String companyName, String contactName, String contactTitle, String address, String city, String region, String postalCode, String country, String phone, String fax) {
        this.id_supplier = id_supplier;
        this.nama_suplier = nama_suplier;
        this.alamat= alamat;
        this.kota = kota;
        this.kode_post = kode_post;
        
    }
         public String id_supplier() {
        return id_supplier;
    }
         public void setid_supplier(String id_supplier) {
        this.id_supplier = id_supplier;
    }

    public String getnama_suplier() {
        return nama_suplier;
    }

    public void setnama_suplier(String nama_suplier) {
        this.nama_suplier = nama_suplier;
    }

    public String getalamat() {
        return alamat;
    }

    public void setalamat(String alamat) {
        this.alamat = alamat;
    }

    public String getkota() {
        return kota;
    }

    public void setkota(String kota) {
        this.kota = kota;
    }

    public String kode_post() {
        return kode_post;
    }

    public void setkode_post(String kode_post) {
        this.kode_post = kode_post;
    }

   
}


}
