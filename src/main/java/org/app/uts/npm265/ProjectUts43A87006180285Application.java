package org.app.uts.npm265;

import java.util.List;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ProjectUts43A87006180285Application {

	public static void main(String[] args) {
		SpringApplication.run(ProjectUts43A87006180285Application.class, args);
	}
  public CommandLineRunner testsuplierRepository(suplierRepository repo){
            return( String[] a) -> {
                List<suplier> list = repo.findAll();
                 System.out.format("%-15s %-40s %-30s %-20s \n", "id_supplier","nama_suplier",
                            "alamat","kota","kode_post");
                    System.out.println("--------------------------------------------------------" 
                            + "----------------------------");
                list.forEach((suplier c) ->{
                   System.out.format("%-15s %-40s %-30s %-20s \n",
                            c.id_supplier(),c.getnama_suplier(),
                            c.getalamat(),c.kota(),c.kode_post());      
                });
                    };
        }
}
public CommandLineRunner testInsert(suplierRepository repo){
            return a -> {
                try {
                          suplier cus = new Customers();
              cus.setid_supplier("AABAB");
              cus.setnama_supliere("ari");
              cus.setalamat("setu");
              cus.setkota("bekasi");
              cus.setkode_post("17320");
              
              repo.save(cus);
                System.out.println("Save suplier sukses!");
                } catch (Exception e) {
                    System.out.println("Save suplier gagal!");
                    System.out.println("Eror:"+ e.getMessage());
                }
            };
        }
       
        public CommandLineRunner testUpdatesuplier(suplierRepository repo){
            return a ->{
                suplier cus = repo.findById("AABAB").orElse(null);
                cus.nama_suplier("Agus ari");
                repo.save(cus);
                System.out.println("suplier AABAB ditemukan" + cus.getnama_suplier());
            };
        }
           @Bean
        public CommandLineRunner testDeletesuplier(suplierRepository repo){
            return a ->{
                Customers cus = repo.findById("AABAB").orElse(null);
                if(cus != null)repo.delete(cus);
                System.out.println("suplier AABAB sudah dihapus");
            };
        }
}
